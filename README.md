# FlaskFleet on K8s - Blog Application Setup on Kubernetes
FlaskFleet on K8s is a project that demonstrates how to deploy a [Flask-based blog application](https://flask.palletsprojects.com/en/2.3.x/) on Kubernetes. This project showcases various aspects of DevOps and Cloud deployment, including Dynamic/Static Volume provisioning, initContainer usage, sensitive data handling, and application health checks in Kubernetes and Docker.

## Prerequisites
To get started with this porject, you'll need:

### AWS Setup
- **AWS Account**: Create a free AWS account [here](https://aws.amazon.com/free/)
- **IAM Role Creation**: Follow this [guide](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create.html) to create an IAM role with administrative access
This project utilizes an [AWS template](https://github.com/aws-samples/amazon-efs-developer-zone/tree/main/application-integration/container/eks#setup-the-working-environment) for EKS cluster setup. Alternatively, for a more hands-on approach, you can use a self-managed cluster with the project [KubeFlow Automator](https://gitlab.com/aliceyiu0307/KubeFlow-Automator).

## Key Features
- **Containerization with Docker**: Details process of containerizing the Flask blog application
- **Kubernetes Deployment**: Walkthrough of deploying the application on Kubernetes, including deployment files and configurations
- **Persistent Storage with AWS EFS**: Integration of AWS EFS for persistent storage, ensuring data persistence acroess pod restarts or cluster crashes
- **Sealed Secrets**: Utilization of Sealed Secrets for secure handling of sensitive data
- **initContainers**: Utilization of initContainers to prepare and set up the environment, in this project, initializing the database
- **Auto Scaling**: Implemention of auto-scaling in Kubernetes to efficiently manage resource utilization
- **Health Checks**: Setup and configuration of health checks to ensure application reliability

## Getting Started
1. Use this [AWS template](https://github.com/aws-samples/amazon-efs-developer-zone/tree/main/application-integration/container/eks#setup-the-working-environment) to set up a EKS cluster and node groups (Note: you can choose other operating system for your perference, adjust the installation command as needed, e.g. Change from `sudo yum install -y jq` to `sudo apt install -y jq` for Ubuntu). It takes about 15-20 minutes to launch the EKS cluster
2. Modify the `managedNodeGroups` section in step 4 under `Creating the EKS Cluster using eksctl` to suit your requirements. For example:
```
managedNodeGroups:
# adjust here
- name: nodegroup
  minSize: 1
  desiredCapacity: 2
  maxSize: 3
  instanceType: t2.medium
  ssh:
    enableSsm: true
```
3. Use `kubectl get nodes` to list all the available nodes
4. Follow the guide to complete the cluster setup

## Build Image Using Dockerfile
1. Build the Docker image using Dockerfile:
```
docker build -t <your-docker-account-id>/<image-name>:<tag-number> .
```
example:
```
docker build -t example123/flask-blog:1.0 .
```

## Install Helm and Sealed Secrets
1. Install Helm and Sealed Secrets
- Install Seal Secret Helm Chart using Helm. Details of Helm installation [here](https://helm.sh/docs/intro/install/)
```
helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
helm install sealed-secrets -n kube-system --set-string fullnameOverride=sealed-secrets-controller sealed-secrets/sealed-secrets
KUBESEAL_VERSION='' # Set this to, for example, KUBESEAL_VERSION='0.23.0'
wget "https://github.com/bitnami-labs/sealed-secrets/releases/download/v${KUBESEAL_VERSION:?}/kubeseal-${KUBESEAL_VERSION:?}-linux-amd64.tar.gz"
tar -xvzf kubeseal-${KUBESEAL_VERSION:?}-linux-amd64.tar.gz kubeseal
sudo install -m 755 kubeseal /usr/local/bin/kubeseal
```
- Use `helm list` to review the repository

2. Provide Extra Security Layer Using Sealed Secrets
Below will use sealed secrets for Docker login credentails.
- In the Kubernetes directory, create 2 new files:
```
.dockerconfigjson
docker-secret.yaml
```
In `.dockerconfigjson`:
```
{
  "auths": {
    "https://index.docker.io/v1/": {
      "username": "you_docker_username",
      "password": "your_docker_login_pw",
      "email": "registered_email",
      "auth": "base64_encoded_credentials"
    }
  }
}
```
Use the following command to encode the `auth`credential:
```
echo '<username>:<password>' | base64
```
Store the result in `auth`.

In `docker-secret.yaml`:
```
apiVersion: v1
kind: Secret
metadata:
  name: my-docker-creds
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: <base64_encoded_.dockerconfigjson>
```
Use the following command to encode `.dockerconfigjson` file:
```
base64 -i .dockerconfigjson
```
Store the result in `docker-secret.yaml`.
- Seal Secret Using `kubeseal`:
```
kubeseal < docker-secret.yaml > sealed-docker-secret.yaml
```
You will get the sealed-docker-secret.yaml file.
- Apply the secret using `kubectl`:
```
kubectl apply -f sealed-docker-secret.yaml
```
Use `kubectl get secret` or `kubectl get sealedsecrets` to review the details.

## Debugging for Sealed Secret
If any problem arises from using `kubeseal`, consider using the following approach:
```
kubectl get pod -n kube-system
kubectl logs pod <sealed-secrets-controller-name> -n kube-system
#note the cert here
vim seal-secret-cert.pem
kubeseal --cert seal-secret-cert.pem < secret.yaml > sealed-secret.yaml
```

## AWS EFS Configuration
Configure AWS EFS as the backend storage for the application. More details [here](https://github.com/aws-samples/amazon-efs-developer-zone/tree/main/application-integration/container/eks/dynamic_provisioning).
```
git clone https://github.com/aws-samples/amazon-efs-developer-zone.git
export CLUSTER_NAME=efsworkshop-eksctl
eksctl utils associate-iam-oidc-provider --cluster $CLUSTER_NAME --approve 
cd /home/ubuntu/environment/amazon-efs-developer-zone/application-integration/container/eks/dynamic_provisioning
pip install -r requirements.txt
python auto-efs-setup.py --region $AWS_REGION --cluster $CLUSTER_NAME --efs_file_system_name myEFSDP
kubectl get sc
```
In the `pv.yaml` and `pvc.yaml`, adjust the `volumeHandle` and `storage` details as needed.

## Debugging for AWS EFS Configuration
Ensure the following items have been configured if any issues arised from the blog application and EFS:
- Mount Target: Mount target is requred as it acts as a bridge between the EFS and the EC2 instance
- Security Groups: Properly configured security groups are required for both the mount target and the EC2 instances to allow NFS traffic. Make sure that the security group attached to the mount target allows inbound traffic on NFS port 2049 from the EC2 instances
- EFS Client: The EC2 instance need the EFS client installed and properly configured to mount the EFS file system
```
#check if the EFS has mount target
aws efs describe-mount-targets --file-system-id fs-<EFS_ID>
#check security groups
aws ec2 describe-security-groups --group-ids sg-<sg_ID>
```

## Apply the yaml Manifests Files in Kubernetes
```
kubectl apply -f flaskr-blog.yaml
kubectl apply -f persistent-volume-claim.yaml
kubectl apply -f pv.yaml
#To review the deployed details:
kubectl get pvc
kubectl get pv
kubectl get pod
```

## Open the Blog Application in Browser
```
<public_IP_address>:<port_number>
```
The port number is 30007 noted in the `kubernetes/flaskr-blog.yaml` file. Adjust it as needed. Be aware that you may also need to open port 30007 in the security groups configuration of the EC2 instance.

## Further Development
1. CI/CD Pipeline Integration: Tools like Jenkins, GitLab CI/CD, or GitHub Actions could be leveraged for deployment automation, making it more efficient and less prone to human error
2. Monitoring & Logging: Prometheus and Grafana for metrics collection and visualization would provide deeper insights into the application's performance and health
3. Database Optimization: Migrating from SQLite to a more robust database system like PostgreSQL or MongoDB, especially for handling larger datasets
4. Traffic Load Balancing: Implement an Application Load Balancer in conjunction with Kubernetes Ingress resources
