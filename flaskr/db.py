import sqlite3, logging, sys

import click
from flask import current_app, g

logging.basicConfig(level=logging.INFO)

def get_db():
    if 'db' not in g:
        db_path = current_app.config['DATABASE']
        logging.info(f"Connecting to database at: {db_path}")
        g.db = sqlite3.connect(
            db_path,
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

def init_db():
    db = get_db()
    try:
        with current_app.open_resource('schema.sql') as f:
            sql_script = f.read().decode('utf8')
            logging.info(f"fExecuting SQL script: {sql_script}")
            db.executescript(sql_script)
        logging.info("Database tables created successfully.")
    except Exception as e:
        logging.error(f"Error executing SQL script: {e}")
        raise

@click.command('init-db')
def init_db_command():
    """Clear the existing data and create new tables."""
    try:
        init_db()
        click.echo('Initialized the database.')
    except Exception as e:
        logging.error(f"Database initialization failed: {e}")
        sys.exit(1)

def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)


