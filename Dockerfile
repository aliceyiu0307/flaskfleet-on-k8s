FROM python

WORKDIR /app

COPY ./flaskr /app/flaskr
COPY requirements.txt /app/

RUN pip install --no-cache-dir -r ./requirements.txt
RUN apt-get update && apt-get install -y sqlite3

EXPOSE 5000

CMD ["flask", "--app", "flaskr:create_app", "run", "--host=0.0.0.0", "--debug"]

